//Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

//

//------------------------------------------------------------------------------------
const filmUrl = "https://ajax.test-danit.com/api/swapi/films";

fetch(filmUrl,{
    method: "GET"
})
    .then(data =>{
        if (!data.ok) {
            throw new Error('Network response was not ok');
        }
        return data.json();
    })
    .then(films =>{
        displayFilmsList(films);
        console.log(films);

        films.forEach(film => {
            charactersInfo(film.characters, film.episodeId);
        });
    })
    .catch(error =>{
        console.error("Error", error);
    })

function displayFilmsList(films){
    const filmList = document.querySelector(".main");

    films.forEach(film => {
        let filmCard = document.createElement("div");
        filmCard.classList.add("filmCard");
        filmCard.setAttribute("film-id", film.episodeId);
        const loader = document.createElement("div");
        loader.classList.add("loader");

        let filmCard_title = document.createElement("div")
        filmCard_title.classList.add("filmCard_title");

        let episodeTitle = document.createElement("h2");
        episodeTitle.textContent = `Episode ${film.episodeId}: ${film.name}`;
    
        let openingCrawl = document.createElement("p");
        openingCrawl.textContent = film.openingCrawl;

        filmCard_title.append(episodeTitle,openingCrawl);
        filmCard.append(filmCard_title, loader);

        filmList.append(filmCard);
    })
}

function charactersInfo(charactersUrls, episodeId) {
    let charactersData = [];

    Promise.all(charactersUrls.map(url => fetch(url).then(response => response.json())))
        .then(characters => {
            characters.forEach(character => {
            charactersData.push(character.name);
        });
        const targetFilm = document.querySelector(`.filmCard[film-id="${episodeId}"]`)
        const loaderElement = targetFilm.querySelector('.loader');
        let pElement = document.createElement("p");
        pElement.textContent = "Characters: " + JSON.stringify(charactersData).slice(1, -1).replace(/"/g, ' ');
        
        loaderElement.remove();
        targetFilm.append(pElement);
        })
        .catch(error => {
        console.error("Error fetching characters:", error);
        });
}
